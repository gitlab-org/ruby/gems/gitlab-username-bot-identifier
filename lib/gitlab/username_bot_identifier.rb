# frozen_string_literal: true

module Gitlab
  # Determines whether a GitLab username appears to be a bot based on known patterns
  class UsernameBotIdentifier
    KNOWN_GITLAB_COM_BOT_USERNAMES = %w[
      codeowner-maintainer-or-manager
      employment-bot
      gitlab-argo-bot
      gitlab-bot
      gitlab-crowdin-bot
      gitlab-dependency-bot
      gitlab-dependency-update-bot
      gitlab_devrel_bot
      gitlab-duo-code-reviewer
      gitlab-infra-mgmt-bot
      gitlab-jh-bot
      gitlab-llm-bot
      gitlab-qa
      gitlab-release-tools-bot
      gitlab-security-bot
      gitlabduo
      gitlabreviewerrecommenderbot
      gl-infra-danger-bot
      glrenovatebot
      gl-support-bot
      kubitus-bot
      mr-bot
      ops-gitlab-net
      taucher2003-bot
    ].freeze

    # Automatically assigned to orphan records (e.g. when a user is deleted)
    GHOST_ACCOUNT = 'ghost1'

    # Can be spoofed (e.g. someone can register project_1_bot, project_2_bot_abc123)
    PROJECT_ACCESS_TOKEN_REGEX = /^project_\d+_bot_?\w*$/.freeze
    GROUP_ACCESS_TOKEN_REGEX = /^group_\d+_bot_?\w*$/.freeze
    SERVICE_ACCOUNT_REGEX = /^service_account_group_\d+_?\w*$/.freeze

    # Used as best practice by GitLab team members when creating "service accounts"
    KNOWN_SERVICE_ACCOUNT_REGEX = /^gl-service-[-\w]+$/.freeze

    def initialize(username)
      @username = username
    end

    def username
      @username.downcase
    end

    def known_bot?
      KNOWN_GITLAB_COM_BOT_USERNAMES.include?(username)
    end

    def ghost?
      username == GHOST_ACCOUNT
    end

    # "known service accounts" are accounts that match the naming convention of service accounts
    # registered by GitLab team members. These accounts are regular user accounts that are used
    # in automations.
    def known_service_account?
      username.match?(KNOWN_SERVICE_ACCOUNT_REGEX)
    end

    def project_access_token?
      username.match?(PROJECT_ACCESS_TOKEN_REGEX)
    end

    def group_access_token?
      username.match?(GROUP_ACCESS_TOKEN_REGEX)
    end

    # "service accounts" are accounts that were created through the service accounts feature.
    # See https://docs.gitlab.com/ee/user/profile/service_accounts.html
    def service_account?
      username.match?(SERVICE_ACCOUNT_REGEX)
    end

    def project_or_group_access_token?
      project_access_token? ||
        group_access_token?
    end

    def bot?
      known_bot? ||
        known_service_account? ||
        project_or_group_access_token? ||
        service_account?
    end

    def ignorable_account?
      known_bot? ||
        ghost? ||
        known_service_account? ||
        project_or_group_access_token? ||
        service_account?
    end
  end
end
