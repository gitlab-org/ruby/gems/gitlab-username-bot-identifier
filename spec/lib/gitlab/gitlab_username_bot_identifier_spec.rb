# frozen_string_literal: true

require_relative '../../../lib/gitlab/username_bot_identifier'

# rubocop:disable Metrics/BlockLength
RSpec.describe Gitlab::UsernameBotIdentifier do
  # rubocop:disable Lint/ConstantDefinitionInBlock
  GROUP_ACCESS_TOKENS = %w[
    group_9970_bot_d62dd6cbd5e34c2fd296b5928dfd6801
    group_60717473_bot_f416f4e026f052144def99d976488a1b
    group_60717473_bot
  ].freeze
  KNOWN_BOTS = %w[
    gitlab-crowdin-bot
    gitlab-bot
    employment-bot
    Taucher2003-Bot
  ].freeze
  PROJECT_ACCESS_TOKENS = %w[
    project_44798062_bot_24b45236f6ea394d003f4e6804fa1e6a
    project_45093868_bot_f805492040427b2486fbdbbd93347cc8
    project_41372369_bot1
  ].freeze
  SERVICE_ACCOUNTS = %w[
    service_account_group_60717473_b38ef9f68484407c811eba4a5088bc85
    service_account_group_60717473_3f27c0b7cec844285a54381e5d91df38
  ].freeze
  KNOWN_SERVICE_ACCOUNTS = %w[
    gl-service-security
    gl-service-inventory-management
  ].freeze
  REAL_USERNAMES = %w[
    leetickett-gitlab
    daniel-murphy
    stingrayza
    Taucher2003
  ].freeze
  GHOST_ACCOUNT = 'ghost1'
  ALL_USERNAMES = GROUP_ACCESS_TOKENS |
                  KNOWN_BOTS |
                  PROJECT_ACCESS_TOKENS |
                  SERVICE_ACCOUNTS |
                  KNOWN_SERVICE_ACCOUNTS |
                  REAL_USERNAMES |
                  [GHOST_ACCOUNT]
  # rubocop:enable Lint/ConstantDefinitionInBlock

  RSpec.shared_examples 'bool method check' do |method_name, username, expectation|
    it "returns #{expectation} for #{method_name} with username #{username}" do
      expect(described_class.new(username).public_send(method_name)).to be(expectation)
    end
  end

  it '#KNOWN_GITLAB_COM_BOT_USERNAMES only contains lowercase usernames', :aggregate_failures do
    described_class::KNOWN_GITLAB_COM_BOT_USERNAMES.each do |username|
      expect(username).to eq(username.downcase)
    end
  end

  describe 'known_bot?' do
    KNOWN_BOTS.each do |username|
      include_examples 'bool method check', :known_bot?, username, true
    end
    (ALL_USERNAMES - KNOWN_BOTS).each do |username|
      include_examples 'bool method check', :known_bot?, username, false
    end
  end

  describe 'ghost?' do
    include_examples 'bool method check', :ghost?, GHOST_ACCOUNT, true
    (ALL_USERNAMES - [GHOST_ACCOUNT]).each do |username|
      include_examples 'bool method check', :ghost?, username, false
    end
  end

  describe 'known_service_account?' do
    KNOWN_SERVICE_ACCOUNTS.each do |username|
      include_examples 'bool method check', :known_service_account?, username, true
    end
    (ALL_USERNAMES - KNOWN_SERVICE_ACCOUNTS).each do |username|
      include_examples 'bool method check', :known_service_account?, username, false
    end
  end

  describe 'project_access_token?' do
    PROJECT_ACCESS_TOKENS.each do |username|
      include_examples 'bool method check', :project_access_token?, username, true
    end
    (ALL_USERNAMES - PROJECT_ACCESS_TOKENS).each do |username|
      include_examples 'bool method check', :project_access_token?, username, false
    end
  end

  describe 'group_access_token?' do
    GROUP_ACCESS_TOKENS.each do |username|
      include_examples 'bool method check', :group_access_token?, username, true
    end
    (ALL_USERNAMES - GROUP_ACCESS_TOKENS).each do |username|
      include_examples 'bool method check', :group_access_token?, username, false
    end
  end

  describe 'service_account?' do
    SERVICE_ACCOUNTS.each do |username|
      include_examples 'bool method check', :service_account?, username, true
    end
    (ALL_USERNAMES - SERVICE_ACCOUNTS).each do |username|
      include_examples 'bool method check', :service_account?, username, false
    end
  end

  describe 'project_or_group_access_token?' do
    project_and_group_access_tokens = PROJECT_ACCESS_TOKENS + GROUP_ACCESS_TOKENS
    project_and_group_access_tokens.each do |username|
      include_examples 'bool method check', :project_or_group_access_token?, username, true
    end
    (ALL_USERNAMES - project_and_group_access_tokens).each do |username|
      include_examples 'bool method check', :project_or_group_access_token?, username, false
    end
  end

  describe 'bot?' do
    bots = ALL_USERNAMES - (REAL_USERNAMES + [GHOST_ACCOUNT])
    bots.each do |username|
      include_examples 'bool method check', :bot?, username, true
    end
    (ALL_USERNAMES - bots).each do |username|
      include_examples 'bool method check', :bot?, username, false
    end
  end

  describe 'ignorable_account?' do
    (ALL_USERNAMES - REAL_USERNAMES).each do |username|
      include_examples 'bool method check', :ignorable_account?, username, true
    end
    REAL_USERNAMES.each do |username|
      include_examples 'bool method check', :ignorable_account?, username, false
    end
  end
end
# rubocop:enable Metrics/BlockLength
