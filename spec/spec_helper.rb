# frozen_string_literal: true

require 'simplecov-cobertura'

RSpec.configure do |config|
  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([SimpleCov::Formatter::CoberturaFormatter])
  SimpleCov.start do
    load_profile 'rails'
    add_filter 'vendor'
  end

  config.expect_with :rspec do |r|
    r.max_formatted_output_length = nil
  end
end
