Please describe what you have changed and why:



If you want this change to create a changelog entry use a changelog commit trailer.

/cc @gitlab-org/developer-relations/contributor-success
