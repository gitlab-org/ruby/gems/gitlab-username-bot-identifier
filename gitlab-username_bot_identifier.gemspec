# frozen_string_literal: true

require_relative 'lib/gitlab/username_bot_identifier/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab-username_bot_identifier'
  spec.version = UsernameBotIdentifier::VERSION
  spec.authors = ['Lee Tickett']
  spec.email = ['ltickett@gitlab.com']

  spec.summary = 'Parse GitLab usernames to determine whether they appear to be bots.'
  spec.homepage = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-username_bot_identifier'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-username_bot_identifier'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-username_bot_identifier/-/releases'
  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.files = [
    'gitlab-username_bot_identifier.gemspec',
    'lib/gitlab/username_bot_identifier.rb'
  ]
end
